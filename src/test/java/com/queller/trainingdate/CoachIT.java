package com.queller.trainingdate;

import com.queller.coach.Coach;
import com.queller.trainingdate.certification.NoCertificationPolicy;
import org.junit.Test;

import java.util.ArrayList;

public class CoachIT {

    @Test
    public void name() {

        Coach coach1 = new Coach();
        coach1.setId(12);
        coach1.setTrainingDates(new ArrayList<>());
        Coach coach2 = new Coach();
        coach2.setId(13);
        coach2.setTrainingDates(new ArrayList<>());

        TrainingDate trainingDate = new TrainingDate();
        trainingDate.setCertificationPolicy(new NoCertificationPolicy());

        trainingDate.setCoach(coach1);
        System.out.println(trainingDate.getCoach() == coach1);
        System.out.println(coach1.getTrainingDates());

        trainingDate.setCoach(coach2);


        System.out.println(trainingDate.getCoach() == coach2);
        System.out.println(coach1.getTrainingDates());
        System.out.println(coach2.getTrainingDates());

    }
}