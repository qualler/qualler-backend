package com.queller.streaming;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.queller.trainingdate.organization.JsonOrganizationWay;
import lombok.Getter;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
public class JsonInternetOrganizationWay implements JsonOrganizationWay {

    @NotNull
    @NotEmpty
    @URL
    private final String videoStreamUrl;

    @JsonCreator
    public JsonInternetOrganizationWay(@JsonProperty("video_stream_url") String videoStreamUrl) {
        this.videoStreamUrl = videoStreamUrl;
    }

    public JsonInternetOrganizationWay(InternetOrganizationWay organizationWay) {
        this.videoStreamUrl = organizationWay.getVideoStreamUrl();
    }
}
