package com.queller.streaming;

import com.queller.money.Money;
import com.queller.trainingdate.organization.OrganizationWay;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import java.util.Currency;

@Getter
@Setter
@Entity
public class InternetOrganizationWay extends OrganizationWay {

    private String videoStreamUrl;

    public InternetOrganizationWay(String videoStreamUrl) {
        this.videoStreamUrl = videoStreamUrl;
    }

    public InternetOrganizationWay() {
    }


    @Override
    public Money calcDailyCosts() {
        return new Money(0, 0, Currency.getInstance("PLN"));
    }
}
