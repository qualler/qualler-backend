package com.queller.address;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import javax.validation.constraints.NotBlank;

@Getter
public class JsonAddress {

    private final String firstLine;
    @NotBlank
    private final String secondLine;
    @NotBlank
    private final String zipCode;
    @NotBlank
    private final String city;

    @JsonCreator
    public JsonAddress(@JsonProperty("first_line") String firstLine,
                       @JsonProperty("second_line") String secondLine,
                       @JsonProperty("zip_code") String zipCode,
                       @JsonProperty("city") String city) {
        this.firstLine = firstLine;
        this.secondLine = secondLine;
        this.zipCode = zipCode;
        this.city = city;
    }

    public JsonAddress(Address address) {
        this.firstLine = address.getFirstLine();
        this.secondLine = address.getSecondLine();
        this.zipCode = address.getZipCode();
        this.city = address.getCity();
    }

    public Address asAddress() {
        return new Address(firstLine, secondLine, zipCode, city);
    }
}
