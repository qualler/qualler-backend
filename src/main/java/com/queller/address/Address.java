package com.queller.address;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.util.Objects;

@Embeddable
@Getter
@Setter
public class Address {

    private String firstLine;
    private String secondLine;
    private String zipCode;
    private String city;

    public Address(String firstLine, String secondLine, String zipCode, String city) {
        this.firstLine = firstLine;
        this.secondLine = secondLine;
        this.zipCode = zipCode;
        this.city = city;
    }

    public Address() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Address)) return false;
        Address address = (Address) o;
        return Objects.equals(getFirstLine(), address.getFirstLine()) &&
                Objects.equals(getSecondLine(), address.getSecondLine()) &&
                Objects.equals(getZipCode(), address.getZipCode()) &&
                Objects.equals(getCity(), address.getCity());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getFirstLine(), getSecondLine(), getZipCode(), getCity());
    }
}
