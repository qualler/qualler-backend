package com.queller.training;

import com.queller.trainingdate.TrainingDate;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
public class Training {

    @Id
    @GeneratedValue
    private Integer id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false, length = 4096)
    private String description;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderColumn(name = "ordinal")
    private List<TrainingModule> modules;
    @OneToMany(mappedBy = "training")
    private List<TrainingDate> trainingDates;

    public Training() {
    }

    public Training(String name, String description, List<TrainingModule> modules) {
        this.name = name;
        this.description = description;
        this.modules = modules;
    }

    public List<TrainingModule> getModules() {
        if (modules == null)
            this.modules = new ArrayList<>();
        return Collections.unmodifiableList(modules);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Training)) return false;
        Training training = (Training) o;
        return Objects.equals(getId(), training.getId());
    }

    public List<TrainingDate> getTrainingDates() {
        return Collections.unmodifiableList(trainingDates);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    public void addDate(TrainingDate trainingDate) {
        if (trainingDates.contains(trainingDate)) {
            return;
        }
        trainingDates.add(trainingDate);
        trainingDate.setTraining(this);
    }

    public void transferDate(TrainingDate trainingDate, Training training) {
        if (!trainingDates.contains(trainingDate)) {
            return;
        }
        trainingDates.remove(trainingDate);
        training.addDate(trainingDate);
        trainingDate.setTraining(training);
    }
}
