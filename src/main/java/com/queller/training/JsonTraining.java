package com.queller.training;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import javax.validation.constraints.NotBlank;

@Getter
public class JsonTraining {

    @NotBlank
    private final String name;
    @NotBlank
    private final String description;

    @JsonCreator
    public JsonTraining(@JsonProperty("name") String name,
                        @JsonProperty("description") String description) {
        this.name = name;
        this.description = description;
    }

    public JsonTraining(Training training) {
        this.name = training.getName();
        this.description = training.getDescription();
    }
}
