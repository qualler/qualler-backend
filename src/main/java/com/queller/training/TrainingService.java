package com.queller.training;

import com.google.common.collect.ImmutableList;
import com.queller.trainingdate.DetailedJsonTrainingDate;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@Validated
@AllArgsConstructor
public class TrainingService {

    private final TrainingRepository trainingRepository;

    public void createTraining(@Valid JsonTraining jsonTraining) {
        Training training = new Training(jsonTraining.getName(), jsonTraining.getDescription(), new ArrayList<>());
        trainingRepository.save(training);
    }

    public void updateTraining(int trainingId, @Valid JsonTraining jsonTraining) {
        Training training = trainingRepository.getOne(trainingId);
        training.setName(jsonTraining.getName());
        training.setDescription(jsonTraining.getDescription());
    }

    public List<DetailedJsonTraining> listTrainings() {
        return trainingRepository.findAll().stream()
                .map(DetailedJsonTraining::new)
                .collect(Collectors.toList());
    }

    public List<DetailedJsonTrainingDate> listTrainingDates(int trainingId) {
        return trainingRepository.findById(trainingId)
                .map(training -> training.getTrainingDates().stream()
                        .map(DetailedJsonTrainingDate::new)
                        .collect(Collectors.toList()))
                .orElse(ImmutableList.of());
    }
}
