package com.queller.training;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Getter;

@Getter
public class DetailedJsonTraining {

    private final int id;
    @JsonUnwrapped
    private final JsonTraining training;

    public DetailedJsonTraining(Training training) {
        this.id = training.getId();
        this.training = new JsonTraining(training);
    }
}
