package com.queller.training;

import com.queller.trainingdate.DetailedJsonTrainingDate;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/trainings")
public class TrainingController {

    private final TrainingService trainingService;

    @GetMapping
    public List<DetailedJsonTraining> listTrainings() {
        return trainingService.listTrainings();
    }

    @GetMapping("/{id}/training-dates")
    public List<DetailedJsonTrainingDate> trainingDates(@PathVariable("id") int id) {
        return trainingService.listTrainingDates(id);
    }

    @PostMapping
    public void createTraining(@RequestBody JsonTraining training) {
        trainingService.createTraining(training);
    }

    @PutMapping("/{trainingId}")
    public void updateTraining(@PathVariable("trainingId") int trainingId,
                               @RequestBody JsonTraining training) {
        trainingService.updateTraining(trainingId, training);
    }

}
