package com.queller.trainingdate;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.queller.money.JsonMoney;
import com.queller.trainingdate.certification.JsonCertificationPolicy;
import com.queller.trainingdate.organization.JsonOrganizationWay;
import lombok.Getter;

import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class JsonTrainingDate {

    private final int trainingId;
    private final int coachId;

    @NotNull
    private final LocalDate startDate;
    @NotNull
    private final LocalDate endDate;

    private final boolean open;
    @NotNull
    @Valid
    private final JsonMoney price;
    @NotNull
    @Valid
    private final List<JsonOrganizationWay> organizationWays;
    @NotNull
    @Valid
    private final JsonCertificationPolicy certificationPolicy;

    @JsonCreator
    public JsonTrainingDate(@JsonProperty("training_id") int trainingId,
                            @JsonProperty("coach_id") int coachId,
                            @JsonProperty("start_date") LocalDate startDate,
                            @JsonProperty("end_date") LocalDate endDate,
                            @JsonProperty("open") boolean open,
                            @JsonProperty("price") JsonMoney price,
                            @JsonProperty("organization_ways") List<JsonOrganizationWay> organizationWays,
                            @JsonProperty("certification_policy") JsonCertificationPolicy certificationPolicy) {
        this.trainingId = trainingId;
        this.coachId = coachId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.open = open;
        this.price = price;
        this.organizationWays = organizationWays;
        this.certificationPolicy = certificationPolicy;
    }


    public JsonTrainingDate(TrainingDate date) {
        this.trainingId = date.getTraining().getId();
        this.coachId = date.getCoach().getUser().getId();
        this.startDate = date.getStartDate();
        this.endDate = date.getEndDate();
        this.open = date.isOpen();
        this.price = new JsonMoney(date.getPrice());
        this.organizationWays = date.getOrganizationWays().stream()
                .map(JsonOrganizationWay::of)
                .collect(Collectors.toList());
        this.certificationPolicy = JsonCertificationPolicy.of(date.getCertificationPolicy());
    }

    @AssertTrue
    public boolean datesAreValid() {
        return endDate.isAfter(startDate);
    }
}
