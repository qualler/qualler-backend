package com.queller.trainingdate.organization;

import com.queller.money.Money;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

/**
 * Sposób organizacji szkolenia.
 * <p>
 * Określa medium przekazu treści szkolenia do odbiorców ({@link com.queller.trainingdate.participant.Participant})
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Getter
@Setter
public abstract class OrganizationWay {

    @Id
    @GeneratedValue
    private Integer id;

    /**
     * Metoda obliczjaca dzienny koszt organizacji szkolenia za pomocą wybranego sposobu
     * @return dzienny koszt organizacji
     */
    public abstract Money calcDailyCosts();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrganizationWay)) return false;
        OrganizationWay that = (OrganizationWay) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId());
    }
}
