package com.queller.trainingdate.organization;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.queller.location.JsonLocationOrganizationWay;
import com.queller.location.LocationOrganizationWay;
import com.queller.streaming.InternetOrganizationWay;
import com.queller.streaming.JsonInternetOrganizationWay;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = JsonInternetOrganizationWay.class, name = "internet"),
        @JsonSubTypes.Type(value = JsonLocationOrganizationWay.class, name = "location"),
})
public interface JsonOrganizationWay {

    static JsonOrganizationWay of(OrganizationWay organizationWay) {
        if (organizationWay instanceof LocationOrganizationWay) {
            return new JsonLocationOrganizationWay(((LocationOrganizationWay) organizationWay));
        } else if (organizationWay instanceof InternetOrganizationWay) {
            return new JsonInternetOrganizationWay(((InternetOrganizationWay) organizationWay));
        }
        throw new IllegalArgumentException();
    }
}
