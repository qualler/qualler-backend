package com.queller.trainingdate;

import com.queller.coach.Coach;
import com.queller.location.LocationOrganizationWay;
import com.queller.money.Money;
import com.queller.training.Training;
import com.queller.trainingdate.certification.CertificationPolicy;
import com.queller.trainingdate.certification.CertificationPolicyViolatedException;
import com.queller.trainingdate.organization.OrganizationWay;
import com.queller.trainingdate.participant.Participation;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

/**
 * Termin szkolenia.
 */
@Getter
@Entity
@Setter
public class TrainingDate {

    private static final long MAX_TRAINING_DURATION = 10;

    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne(optional = false)
    private Training training;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private TrainingDateState state;

    @Column(nullable = false)
    private LocalDate startDate;

    @Column(nullable = false)
    private LocalDate endDate;
    private boolean open;

    private Money price;

    @ManyToOne(optional = false)
    private Coach coach;

    /**
     * Lista sposobów organizacji szkolenia.
     * <p>
     * Szkolenie może być organizowane jako szkolenie konwencjonalne
     * (tj. w wynajętej sali) lub poprzez stream video.
     * <p>
     * Szkolenie może odbywać się tylko w jednej lokalizacji,
     * natomiast może posiadać wiele stream'ów.
     */
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<OrganizationWay> organizationWays;

    /**
     * Polityka certyfikacji szkolenia.
     * <p>
     * Szkolenia certyfikowane nakładają dodatkowe
     * ograniczenia na wybór trenera.
     * <p>
     * Jeżeli szkolenie jest certyfikowane - np. przygotowuje do egzaminu OCJP
     * to prowadzący je trener musi taki certyfikat (lub certyfikaty) posiadać.
     */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private CertificationPolicy certificationPolicy;

    @OneToMany(mappedBy = "trainingDate")
    private List<Participation> participations;

    public TrainingDate() {
    }

    public TrainingDate(Training training,
                        LocalDate startDate,
                        LocalDate endDate,
                        boolean open,
                        Money price,
                        Coach coach,
                        List<OrganizationWay> organizationWays,
                        CertificationPolicy certificationPolicy) {
        setTraining(training);
        setState(TrainingDateState.DRAFT);
        setStartDate(startDate);
        setEndDate(endDate);
        setCertificationPolicy(certificationPolicy);
        setOpen(open);
        setOrganizationWays(organizationWays);
        setCoach(coach);
        setPrice(price);
    }

    /**
     * Metoda ustawiajaca status otwarte/zamknięte na terminie szkolenia.
     * <p>
     * Jeśli szkolenie jest zamknięte - automatycznie zostanie wyczyszczona
     * lista sposobów organizacji.
     *
     * @param open szkolenie otwarte/zamknięte
     */
    public void setOpen(boolean open) {
        this.open = open;
        if (!open)
            this.organizationWays = new ArrayList<>();
    }

    /**
     * Procedura ustaewianie/zmiany szkolenia do którego ma przynależeć termin.
     * <p>
     * Jeżeli termin został już wcześniej przypisany do jakiegoś szkolenia, nastąpi
     * uruchomienie procedury przekazania terminu pomiędzy szkoleniammi.
     *
     * @param training szkolenie do którego ma przynależeć wskazany termin. Nie może być null.
     */
    public void setTraining(Training training) {
        Objects.requireNonNull(training);
        if (this.training == null) {
            this.training = training;
            training.addDate(this);
            return;
        }
        if (!this.training.equals(training)) {
            Training previousTraining = this.training;
            this.training = training;
            previousTraining.transferDate(this, training);
        }
    }

    /**
     * Ustawia sposoby organizacji szkolenia.
     * <p>
     * Dla szkoleń zamniętych metoda nie ma zastosowania
     * i utstawi w miejscu sposobów organizacji pustą listę.
     *
     * @param organizationWays sposoby organizacji szkolenia. Nie może być null.
     * @throws IllegalArgumentException Jeśli na podanej liście sposobów organizacji
     *                                  znajdzie się więcej niż jedna organizacja w lokalizacji
     * @see LocationOrganizationWay
     */
    public void setOrganizationWays(List<OrganizationWay> organizationWays) {
        Objects.requireNonNull(organizationWays);
        if (!open)
            this.organizationWays = new ArrayList<>();
        else {
            long locationOrganizationWays = organizationWays.stream()
                    .filter(Objects::nonNull)
                    .filter(organizationWay -> organizationWay instanceof LocationOrganizationWay)
                    .count();
            if (locationOrganizationWays > 1) {
                throw new IllegalArgumentException("Training date cannot be organized in more than 1 location");
            }
            this.organizationWays = organizationWays.stream()
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }
    }

    public List<OrganizationWay> getOrganizationWays() {
        return Collections.unmodifiableList(organizationWays);
    }

    public void setPrice(Money price) {
        this.price = Objects.requireNonNull(price);
    }

    /**
     * Procedura ustewiania/zmiany trenera który poprowadzi termin szkolenia.
     * <p>
     * Podany trener zostanie sprawdzony pod kątem zgodności z polityką certyfikacji terminu.
     * <p>
     * Jeżeli termin został już wcześniej przypisany do jakiegoś trenera, nastąpi
     * uruchomienie procedury przekazania terminu pomiędzy trenerami.
     *
     * @param coach trener który poprowadzi szkolenie. Nie może być null.
     * @throws CertificationPolicyViolatedException Jeśli trener nie jest zgodny z polityką certyfikacji.
     */
    public void setCoach(Coach coach) {
        Objects.requireNonNull(coach);
        if (!certificationPolicy.coachIsCompliant(coach))
            throw new CertificationPolicyViolatedException();
        if (this.coach == null) {
            this.coach = coach;
            coach.addTraining(this);
            return;
        }
        if (!this.coach.equals(coach)) {
            Coach previousCoach = this.coach;
            this.coach = coach;
            previousCoach.passTraining(this, coach);
        }
    }

    /**
     * Metoda ustawiająca datę zakończenia szkolenia.
     * Szkolenie nie może trwać dłużej niż ilość dni określona w limicie {@link TrainingDate#MAX_TRAINING_DURATION}.
     *
     * @param endDate data zakończenia szkolenia. Nie może być null
     * @throws IllegalArgumentException jeśli ilość dni między datą rozpoczęcia a podaną datą zakończenia przekroczy
     *                                  próg {@link TrainingDate#MAX_TRAINING_DURATION}
     */
    public void setEndDate(LocalDate endDate) {
        Objects.requireNonNull(endDate);
        if (calcDaysOfTraining(endDate) > MAX_TRAINING_DURATION)
            throw new IllegalStateException();
        this.endDate = endDate;
    }

    /**
     * Metoda obliczająca koszty szkolenia.
     * <p>
     * Koszt szkolenia jest liczony jako suma kosztów wynajęcia trenera na ilość dni pomiędzy
     * datą rozpoczęnia a datą rozpoczęcia oraz kosztów wynajęcia sali.
     * <p>
     * Dla szkoleń zamkniętych nie bierzemy pod uwagę kosztów organizacji - uwzględniamy tylko
     * koszt wynajęcia trenera.
     *
     * @return koszt szkolenia
     */
    public Money calcCosts() {
        Money coachCost = coach.getDailySalary().multiply(getDaysOfTraining());
        if (!open) {
            return coachCost;
        }

        Money dailyCosts = organizationWays.stream()
                .map(OrganizationWay::calcDailyCosts)
                .reduce(new Money(0, 0, Currency.getInstance("PLN")), Money::add);
        return dailyCosts.multiply(getDaysOfTraining()).add(coachCost);
    }

    public int getDaysOfTraining() {
        return calcDaysOfTraining(endDate);
    }

    private int calcDaysOfTraining(LocalDate endDate) {
        return (int) DAYS.between(startDate, endDate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TrainingDate)) return false;
        TrainingDate that = (TrainingDate) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    public void addParticipation(Participation participation) {
        Objects.requireNonNull(participation);
        if (participation.getTrainingDate().equals(this)) {
            this.participations.add(participation);
        }
    }
}
