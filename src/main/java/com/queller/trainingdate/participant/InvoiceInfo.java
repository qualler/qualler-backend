package com.queller.trainingdate.participant;

import com.queller.address.Address;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.util.Objects;

@Getter
@Setter
@Embeddable
public class InvoiceInfo {

    private Address address;

    public InvoiceInfo() {
    }

    public InvoiceInfo(Address address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InvoiceInfo)) return false;
        InvoiceInfo that = (InvoiceInfo) o;
        return Objects.equals(getAddress(), that.getAddress());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getAddress());
    }
}
