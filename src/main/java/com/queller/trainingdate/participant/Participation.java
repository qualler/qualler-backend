package com.queller.trainingdate.participant;

import com.queller.trainingdate.TrainingDate;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@IdClass(Participation.ID.class)
@Getter
@Setter
public class Participation {

    @Id
    @ManyToOne
    private Participant participant;
    @Id
    @ManyToOne
    private TrainingDate trainingDate;

    private boolean paid;
    private String certificateUrl;

    public Participation() {
    }

    public Participation(Participant participant, TrainingDate trainingDate, boolean paid, String certificateUrl) {
        this.participant = participant;
        this.trainingDate = trainingDate;
        this.paid = paid;
        this.certificateUrl = certificateUrl;
        trainingDate.addParticipation(this);
        participant.addParticipation(this);
    }

    @Getter
    @Setter
    public static class ID implements Serializable {

        private Integer participant;
        private Integer trainingDate;

        public ID(Integer participant, Integer trainingDate) {
            this.participant = participant;
            this.trainingDate = trainingDate;
        }

        public ID() {
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof ID)) return false;
            ID id = (ID) o;
            return Objects.equals(getParticipant(), id.getParticipant()) &&
                    Objects.equals(getTrainingDate(), id.getTrainingDate());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getParticipant(), getTrainingDate());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Participation)) return false;
        Participation that = (Participation) o;
        return Objects.equals(participant, that.participant) &&
                Objects.equals(trainingDate, that.trainingDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(participant, trainingDate);
    }
}
