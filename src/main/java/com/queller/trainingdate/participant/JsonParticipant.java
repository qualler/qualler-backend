package com.queller.trainingdate.participant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.queller.user.JsonProfile;
import com.queller.user.User;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class JsonParticipant implements JsonProfile {

    @NotNull
    @Valid
    private final JsonInvoiceInfo invoiceInfo;

    @JsonCreator
    public JsonParticipant(@JsonProperty("invoice_info") JsonInvoiceInfo invoiceInfo) {
        this.invoiceInfo = invoiceInfo;
    }

    public JsonParticipant(Participant participant) {
        this.invoiceInfo = new JsonInvoiceInfo(participant.getInvoiceInfo());
    }

    @Override
    public void createProfile(User user) {
        user.createParticipantProfile(invoiceInfo.asInvoiceInfo());
    }
}
