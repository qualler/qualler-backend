package com.queller.trainingdate.participant;

import com.google.common.collect.ImmutableSet;
import com.queller.user.Profile;
import com.queller.user.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Getter
@Setter
public class Participant extends Profile {

    private InvoiceInfo invoiceInfo;

    @OneToMany(mappedBy = "participant")
    private List<Participation> participations;

    public Participant() {
    }

    public Participant(User user, InvoiceInfo invoiceInfo) {
        setInvoiceInfo(invoiceInfo);
        setUser(user);
    }

    public List<Participation> getParticipations() {
        return Collections.unmodifiableList(participations);
    }

    public void setInvoiceInfo(InvoiceInfo invoiceInfo) {
        this.invoiceInfo = Objects.requireNonNull(invoiceInfo);
    }

    @Override
    public Set<String> roles() {
        return ImmutableSet.of("PARTICIPANT");
    }

    public void addParticipation(Participation participation) {
        Objects.requireNonNull(participation);
        if (participation.getParticipant().equals(this)) {
            participations.add(participation);
        }
    }
}
