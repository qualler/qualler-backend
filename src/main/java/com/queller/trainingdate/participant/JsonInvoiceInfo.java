package com.queller.trainingdate.participant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.queller.address.JsonAddress;
import lombok.Getter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Getter
public class JsonInvoiceInfo {

    @NotNull
    @Valid
    private final JsonAddress address;

    @JsonCreator
    public JsonInvoiceInfo(@JsonProperty("address") JsonAddress address) {
        this.address = address;
    }

    public JsonInvoiceInfo(InvoiceInfo invoiceInfo) {
        this.address = new JsonAddress(invoiceInfo.getAddress());
    }

    public InvoiceInfo asInvoiceInfo() {
        return new InvoiceInfo(address.asAddress());
    }
}
