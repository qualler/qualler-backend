package com.queller.trainingdate;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TrainingDateRepository extends JpaRepository<TrainingDate, Integer> {

    @Query("select t from TrainingDate t join t.organizationWays o where o.location.id = :locationId")
    List<TrainingDate> findByLocation(@Param("locationId") int locationId);
}
