package com.queller.trainingdate.certification;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
public class JsonCertificationRequiredPolicy implements JsonCertificationPolicy {

    @NotNull
    @NotEmpty
    private final Set<String> certificates;

    @JsonCreator
    public JsonCertificationRequiredPolicy(@JsonProperty("certificates") Set<String> certificates) {
        this.certificates = certificates;
    }

    public JsonCertificationRequiredPolicy(CertificationRequiredPolicy certificationPolicy) {
        this.certificates = certificationPolicy.getCertificates();
    }

    @Override
    public CertificationPolicy asCertificationPolicy() {
        return new CertificationRequiredPolicy(certificates);
    }
}
