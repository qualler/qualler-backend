package com.queller.trainingdate.certification;

import com.queller.coach.Coach;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import java.util.Set;

@Entity
@Getter
@Setter
public class CertificationRequiredPolicy extends CertificationPolicy {

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<String> certificates;

    public CertificationRequiredPolicy(Set<String> certificates) {
        this.certificates = certificates;
    }

    public CertificationRequiredPolicy() {
    }

    @Override
    public boolean coachIsCompliant(Coach coach) {
        return coach.getCertificates().containsAll(certificates);
    }
}
