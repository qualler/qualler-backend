package com.queller.trainingdate.certification;

import com.queller.coach.Coach;

import javax.persistence.Entity;

@Entity
public class NoCertificationPolicy extends CertificationPolicy {

    @Override
    public boolean coachIsCompliant(Coach coach) {
        return true;
    }
}
