package com.queller.trainingdate.certification;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = JsonNoCertificationPolicy.class, name = "no-certification"),
        @JsonSubTypes.Type(value = JsonCertificationRequiredPolicy.class, name = "certification-required")
})
public interface JsonCertificationPolicy {

    static JsonCertificationPolicy of(CertificationPolicy certificationPolicy) {
        if (certificationPolicy instanceof NoCertificationPolicy)
            return JsonNoCertificationPolicy.of();
        if (certificationPolicy instanceof CertificationRequiredPolicy) {
            return new JsonCertificationRequiredPolicy(((CertificationRequiredPolicy) certificationPolicy));
        }
        throw new IllegalArgumentException();
    }

    CertificationPolicy asCertificationPolicy();

}
