package com.queller.trainingdate.certification;

import com.queller.coach.Coach;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

/**
 * Polityka certyfikacji szkolenia.
 * <p>
 * W przypadku braku certyfikacji należy wybrać {@link NoCertificationPolicy}
 */
@Entity
@Getter
@Setter
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class CertificationPolicy {

    @Id
    @GeneratedValue
    private Integer id;

    /**
     * Metoda sprawdzająca czy trener jest zgodny ze wskazaną
     * polityką certyfikacji.
     *
     * @param coach trener. Nie może być null
     * @return wynik testu zgodności
     */
    public abstract boolean coachIsCompliant(Coach coach);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CertificationPolicy)) return false;
        CertificationPolicy that = (CertificationPolicy) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
