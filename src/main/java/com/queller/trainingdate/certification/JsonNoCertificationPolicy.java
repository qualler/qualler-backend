package com.queller.trainingdate.certification;

import com.fasterxml.jackson.annotation.JsonCreator;

public class JsonNoCertificationPolicy implements JsonCertificationPolicy {

    private static final JsonNoCertificationPolicy INSTANCE = new JsonNoCertificationPolicy();

    @JsonCreator
    public static JsonNoCertificationPolicy of() {
        return INSTANCE;
    }

    private JsonNoCertificationPolicy() {
    }

    @Override
    public CertificationPolicy asCertificationPolicy() {
        return new NoCertificationPolicy();
    }
}
