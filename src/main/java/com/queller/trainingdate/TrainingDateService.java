package com.queller.trainingdate;

import com.queller.coach.Coach;
import com.queller.location.JsonLocationOrganizationWay;
import com.queller.location.Location;
import com.queller.location.LocationOrganizationWay;
import com.queller.location.LocationsRepository;
import com.queller.streaming.InternetOrganizationWay;
import com.queller.streaming.JsonInternetOrganizationWay;
import com.queller.training.Training;
import com.queller.training.TrainingRepository;
import com.queller.trainingdate.organization.JsonOrganizationWay;
import com.queller.trainingdate.organization.OrganizationWay;
import com.queller.user.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@Validated
@AllArgsConstructor
public class TrainingDateService {

    private final TrainingDateRepository trainingDateRepository;
    private final TrainingRepository trainingRepository;
    private final UserRepository userRepository;
    private final LocationsRepository locationsRepository;

    public void createTrainingDate(@Valid JsonTrainingDate jsonTrainingDate) {
        Training training = trainingRepository.getOne(jsonTrainingDate.getTrainingId());
        Coach coach = userRepository.getOne(jsonTrainingDate.getCoachId()).getCoachProfile();

        List<OrganizationWay> organizationWays = asOrganizationWays(jsonTrainingDate.getOrganizationWays());


        TrainingDate trainingDate = new TrainingDate(training,
                jsonTrainingDate.getStartDate(),
                jsonTrainingDate.getEndDate(),
                jsonTrainingDate.isOpen(),
                jsonTrainingDate.getPrice().asMoney(),
                coach,
                organizationWays,
                jsonTrainingDate.getCertificationPolicy().asCertificationPolicy()
        );

        trainingDateRepository.save(trainingDate);
    }

    public List<DetailedJsonTrainingDate> listTrainingDates() {
        return trainingDateRepository.findAll().stream()
                .map(DetailedJsonTrainingDate::new)
                .collect(Collectors.toList());
    }


    private List<OrganizationWay> asOrganizationWays(List<JsonOrganizationWay> jsonOrganizationWays) {
        List<OrganizationWay> organizationWays = new ArrayList<>();
        for (JsonOrganizationWay jsonOrganizationWay : jsonOrganizationWays) {
            if (jsonOrganizationWay instanceof JsonLocationOrganizationWay) {
                int locationId = ((JsonLocationOrganizationWay) jsonOrganizationWay).getLocationId();
                Location location = locationsRepository.getOne(locationId);
                LocationOrganizationWay organizationWay = new LocationOrganizationWay(location);
                organizationWays.add(organizationWay);
            } else if (jsonOrganizationWay instanceof JsonInternetOrganizationWay) {
                String link = ((JsonInternetOrganizationWay) jsonOrganizationWay).getVideoStreamUrl();
                InternetOrganizationWay internetOrganizationWay = new InternetOrganizationWay(link);
                organizationWays.add(internetOrganizationWay);
            }
        }
        return organizationWays;
    }

    public Optional<DetailedJsonTrainingDate> findTrainingDate(int trainingDateId) {
        return trainingDateRepository.findById(trainingDateId)
                .map(DetailedJsonTrainingDate::new);
    }
}
