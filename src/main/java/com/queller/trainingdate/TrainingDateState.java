package com.queller.trainingdate;

public enum TrainingDateState {
    DRAFT, PUBLISHED
}
