package com.queller.trainingdate;

import com.queller.training.DetailedJsonTraining;
import com.queller.user.JsonUser;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
@RequestMapping("/training-dates")
public class TrainingDateController {

    private final TrainingDateService trainingDateService;

    @GetMapping
    public List<DetailedJsonTrainingDate> listTrainings() {
        return trainingDateService.listTrainingDates();
    }

    @GetMapping("/{id}")
    public Optional<DetailedJsonTrainingDate> findTrainingDate(@PathVariable("id") int id) {
        return trainingDateService.findTrainingDate(id);
    }

    @PostMapping
    public void createTrainingDate(@RequestBody @Valid JsonTrainingDate trainingDate) {
        trainingDateService.createTrainingDate(trainingDate);
    }

}
