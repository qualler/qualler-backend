package com.queller.trainingdate;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.queller.training.DetailedJsonTraining;
import com.queller.user.DetailedJsonUser;
import lombok.Getter;

@Getter
public class DetailedJsonTrainingDate {

    private final int id;
    @JsonUnwrapped
    private final JsonTrainingDate trainingDate;
    private final DetailedJsonUser coach;
    private final DetailedJsonTraining training;

    public DetailedJsonTrainingDate(TrainingDate date) {
        this.id = date.getId();
        this.trainingDate = new JsonTrainingDate(date);
        this.coach = new DetailedJsonUser(date.getCoach().getUser());
        this.training = new DetailedJsonTraining(date.getTraining());
    }

}
