package com.queller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuallerApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuallerApplication.class, args);
    }
}
