package com.queller.coach;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.queller.money.JsonMoney;
import com.queller.user.JsonProfile;
import com.queller.user.User;
import lombok.Getter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
public class JsonCoach implements JsonProfile {

    @NotNull
    private final Set<String> certificates;
    @NotNull
    @Valid
    private final JsonMoney dailySalary;

    @JsonCreator
    public JsonCoach(@JsonProperty("certificates") Set<String> certificates,
                     @JsonProperty("daily_salary") JsonMoney dailySalary) {
        this.certificates = certificates;
        this.dailySalary = dailySalary;
    }

    public JsonCoach(Coach coach) {
        this.certificates = coach.getCertificates();
        this.dailySalary = new JsonMoney(coach.getDailySalary());
    }

    @Override
    public void createProfile(User user) {
        user.createCoachProfile(certificates, dailySalary.asMoney());
    }
}
