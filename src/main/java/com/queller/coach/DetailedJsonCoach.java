package com.queller.coach;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

public class DetailedJsonCoach {

    @JsonUnwrapped
    private final JsonCoach coach;

    public DetailedJsonCoach(Coach coach) {
        this.coach = new JsonCoach(coach);
    }

}
