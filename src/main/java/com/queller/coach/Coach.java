package com.queller.coach;

import com.google.common.collect.ImmutableSet;
import com.queller.money.Money;
import com.queller.trainingdate.TrainingDate;
import com.queller.user.Profile;
import com.queller.user.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
public class Coach extends Profile {

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<String> certificates;
    @Column(nullable = false)
    private Money dailySalary;

    @OneToMany(mappedBy = "coach", fetch = FetchType.EAGER, orphanRemoval = true)
    private List<TrainingDate> trainingDates;

    public Coach() {
    }

    public Coach(User user, Set<String> certificates, Money money) {
        setUser(user);
        setCertificates(certificates);
        setDailySalary(money);
    }

    public List<TrainingDate> getTrainingDates() {
        return Collections.unmodifiableList(trainingDates);
    }

    @Override
    public Set<String> roles() {
        return ImmutableSet.of("COACH");
    }

    public Money getWeeklySalary() {
        return dailySalary.multiply(5);
    }

    public void addTraining(TrainingDate trainingDate) {
        if (trainingDates.contains(trainingDate)) {
            return;
        }
        trainingDates.add(trainingDate);
        trainingDate.setCoach(this);
    }

    public void passTraining(TrainingDate trainingDate, Coach coach) {
        if (!trainingDates.contains(trainingDate)) {
            return;
        }
        trainingDates.remove(trainingDate);
        coach.addTraining(trainingDate);
        trainingDate.setCoach(coach);
    }
}
