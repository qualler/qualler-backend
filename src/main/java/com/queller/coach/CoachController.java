package com.queller.coach;

import com.queller.trainingdate.DetailedJsonTrainingDate;
import com.queller.user.DetailedJsonUser;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
public class CoachController {

    private final CoachService coachService;

    @GetMapping("/users/{coachId}/coach/training-dates")
    public List<DetailedJsonTrainingDate> findConductedTrainings(@PathVariable("coachId") int coachId) {
        return coachService.findConductedTrainings(coachId);
    }

    @GetMapping("/users/{coachId}/coach")
    public Optional<DetailedJsonUser> findCoach(@PathVariable("coachId") int coachId) {
        return coachService.findCoach(coachId);
    }


}
