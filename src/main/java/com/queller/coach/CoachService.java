package com.queller.coach;

import com.google.common.collect.ImmutableList;
import com.queller.trainingdate.DetailedJsonTrainingDate;
import com.queller.user.DetailedJsonUser;
import com.queller.user.User;
import com.queller.user.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@Validated
@AllArgsConstructor
public class CoachService {

    private final UserRepository userRepository;

    public Optional<DetailedJsonUser> findCoach(int userId) {
        return userRepository.findById(userId)
                .filter(User::isCoach)
                .map(DetailedJsonUser::new);
    }

    public List<DetailedJsonTrainingDate> findConductedTrainings(int coachId) {
        return userRepository.findById(coachId)
                .map(User::getCoachProfile)
                .map(coach -> coach.getTrainingDates().stream()
                        .map(DetailedJsonTrainingDate::new)
                        .collect(Collectors.toList()))
                .orElse(ImmutableList.of());
    }

}
