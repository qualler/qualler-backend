package com.queller.money;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Currency;
import java.util.Objects;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
public class Money {

    @Column(nullable = false)
    private int wholes;
    @Column(nullable = false)
    private int decimals;
    @Column(nullable = false)
    private Currency currency;

    public Money() {
    }

    public Money multiply(int number) {
        int decimals = this.decimals * number;
        int wholes = this.wholes * number + (decimals / 100);

        return new Money(wholes, decimals % 100, currency);
    }

    @Override
    public String toString() {
        return String.format("%s.%s %s", wholes, decimals, currency.getCurrencyCode());
    }

    public Money add(Money money) {
        Objects.requireNonNull(money);
        if (!currency.equals(money.getCurrency()))
            throw new IllegalArgumentException();

        int decimals = this.decimals + money.getDecimals();
        int wholes = this.wholes + (decimals / 100);

        return new Money(wholes, decimals % 100, currency);
    }
}
