package com.queller.money;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Currency;

@Getter
public class JsonMoney {

    @Min(0)
    private final int wholes;
    @Min(0)
    private final int decimals;
    @NotNull
    private final Currency currency;

    @JsonCreator
    public JsonMoney(@JsonProperty("wholes") int wholes,
                     @JsonProperty("decimals") int decimals,
                     @JsonProperty("currency") Currency currency) {
        this.wholes = wholes;
        this.decimals = decimals;
        this.currency = currency;
    }

    public JsonMoney(Money money) {
        this.wholes = money.getWholes();
        this.decimals = money.getDecimals();
        this.currency = money.getCurrency();
    }

    public Money asMoney() {
        return new Money(wholes, decimals, currency);
    }
}
