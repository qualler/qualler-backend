package com.queller.employee;

import com.queller.user.JsonProfile;
import com.queller.user.User;

public class JsonEmployee implements JsonProfile {

    @Override
    public void createProfile(User user) {
        user.createEmployeeProfile();
    }
}
