package com.queller.employee;

import com.google.common.collect.ImmutableSet;
import com.queller.user.Profile;
import com.queller.user.User;

import javax.persistence.Entity;
import java.util.Set;

@Entity
public class Employee extends Profile {

    public Employee() {
    }

    public Employee(User user) {
        setUser(user);
    }

    @Override
    public Set<String> roles() {
        return ImmutableSet.of("EMPLOYEE");
    }
}
