package com.queller.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Maps;
import com.queller.trainingdate.participant.JsonParticipant;
import lombok.Getter;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
public class JsonUser {

    @NotBlank
    private final String firstName;
    @NotBlank
    private final String lastName;
    @NotBlank
    @Email
    private final String email;

    @NotNull
    @Valid
    @NotEmpty
    private Map<ProfileType, JsonProfile> profiles;

    @JsonCreator
    public JsonUser(@JsonProperty("first_name") String firstName,
                    @JsonProperty("last_name") String lastName,
                    @JsonProperty("email") String email,
                    @JsonProperty("profiles") Map<ProfileType, JsonProfile> profiles) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.profiles = profiles;
    }

    public JsonUser(User user) {
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.email = user.getEmail();
        this.profiles = user.getProfiles().entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> JsonProfile.of(entry.getValue())));
    }
}
