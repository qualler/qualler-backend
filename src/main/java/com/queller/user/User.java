package com.queller.user;

import com.queller.coach.Coach;
import com.queller.employee.Employee;
import com.queller.money.Money;
import com.queller.trainingdate.participant.InvoiceInfo;
import com.queller.trainingdate.participant.Participant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Użytkownik systemu Qualler.
 */
@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
public class User {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    @Column(nullable = false, unique = true)
    private String email;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Map<ProfileType, Profile> profiles;

    public User() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(getId(), user.getId());
    }

    public Set<String> roles() {
        return profiles.values().stream()
                .flatMap(profile -> profile.roles().stream())
                .collect(Collectors.toSet());
    }

    public Map<ProfileType, Profile> getProfiles() {
        if (profiles == null)
            profiles = new HashMap<>();
        return profiles;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    public void createCoachProfile(Set<String> certificates, Money money) {
        if (getProfiles().containsKey(ProfileType.COACH))
            throw new IllegalArgumentException();
        getProfiles().put(ProfileType.COACH, new Coach(this, certificates, money));
    }

    public void createParticipantProfile(InvoiceInfo invoiceInfo) {
        if (getProfiles().containsKey(ProfileType.PARTICIPANT))
            throw new IllegalArgumentException();
        getProfiles().put(ProfileType.COACH, new Participant(this, invoiceInfo));
    }

    public void createEmployeeProfile() {
        getProfiles().put(ProfileType.EMPLOYEE, new Employee(this));
    }

    public Coach getCoachProfile() {
        return (Coach) Optional.ofNullable(getProfiles().get(ProfileType.COACH))
                .orElseThrow(ProfileNotFoundException::new);
    }

    public boolean isCoach() {
        return profiles.containsKey(ProfileType.COACH);
    }
}
