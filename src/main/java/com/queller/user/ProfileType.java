package com.queller.user;

public enum ProfileType {
    EMPLOYEE, COACH, PARTICIPANT
}
