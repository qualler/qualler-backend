package com.queller.user;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Getter;

@Getter
public class DetailedJsonUser {

    private final int id;
    @JsonUnwrapped
    private final JsonUser user;

    public DetailedJsonUser(User user) {
        this.id = user.getId();
        this.user = new JsonUser(user);
    }
}
