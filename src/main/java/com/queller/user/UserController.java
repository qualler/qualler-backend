package com.queller.user;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @GetMapping
    public List<DetailedJsonUser> listUsers() {
        return userService.listUsers();
    }

    @GetMapping("/{profileType}")
    public List<DetailedJsonUser> listUsers(@PathVariable("profileType") ProfileType profileType) {
        return userService.findByProfile(profileType);
    }

    @PostMapping
    public void createUser(@RequestBody JsonUser jsonUser) {
        userService.createUser(jsonUser);
    }

}
