package com.queller.user;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@Validated
@AllArgsConstructor
public class UserService {

    public static final Comparator<DetailedJsonUser> NATURAL_COMPARATOR = Comparator
            .<DetailedJsonUser, String>comparing(user -> user.getUser().getLastName())
            .thenComparing(user -> user.getUser().getFirstName());

    private final UserRepository userRepository;

    public void createUser(@Valid JsonUser jsonUser) {
        User user = User.builder()
                .firstName(jsonUser.getFirstName())
                .lastName(jsonUser.getLastName())
                .email(jsonUser.getEmail())
                .build();

        jsonUser.getProfiles()
                .forEach((profileType, jsonProfile) ->
                        jsonProfile.createProfile(user));
        userRepository.save(user);
    }

    public List<DetailedJsonUser> listUsers() {
        return userRepository.findAll().stream()
                .map(DetailedJsonUser::new)
                .sorted(NATURAL_COMPARATOR)
                .collect(Collectors.toList());
    }

    public List<DetailedJsonUser> findByProfile(ProfileType profileType) {
        return userRepository.findByProfileType(profileType).stream()
                .map(DetailedJsonUser::new)
                .sorted(NATURAL_COMPARATOR)
                .collect(Collectors.toList());
    }


}
