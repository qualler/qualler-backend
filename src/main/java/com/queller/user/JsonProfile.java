package com.queller.user;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.queller.coach.Coach;
import com.queller.coach.JsonCoach;
import com.queller.employee.Employee;
import com.queller.employee.JsonEmployee;
import com.queller.trainingdate.participant.JsonParticipant;
import com.queller.trainingdate.participant.Participant;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = JsonEmployee.class, name = "employee"),
        @JsonSubTypes.Type(value = JsonCoach.class, name = "coach"),
        @JsonSubTypes.Type(value = JsonParticipant.class, name = "participant")
})
public interface JsonProfile {

    static JsonProfile of(Profile profile) {
        if (profile instanceof Employee)
            return new JsonEmployee();
        else if (profile instanceof Coach)
            return new JsonCoach((Coach) profile);
        else if (profile instanceof Participant)
            return new JsonParticipant(((Participant) profile));

        throw new IllegalArgumentException();
    }

    void createProfile(User user);
}
