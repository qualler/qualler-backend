package com.queller.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.annotation.Nullable;
import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {

    @Nullable
    User findByEmail(String email);


    @Query("select u from User u join u.profiles p where key(p) = :profileType")
    List<User> findByProfileType(@Param("profileType") ProfileType profileType);
}
