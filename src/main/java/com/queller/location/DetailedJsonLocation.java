package com.queller.location;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Getter;

@Getter
public class DetailedJsonLocation {

    private final int id;
    @JsonUnwrapped
    private final JsonLocation location;

    public DetailedJsonLocation(Location location) {
        this.id = location.getId();
        this.location = new JsonLocation(location);
    }
}
