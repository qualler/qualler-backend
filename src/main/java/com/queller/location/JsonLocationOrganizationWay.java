package com.queller.location;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.queller.trainingdate.organization.JsonOrganizationWay;
import lombok.Getter;

@Getter
public class JsonLocationOrganizationWay implements JsonOrganizationWay {

    private final int locationId;

    @JsonCreator
    public JsonLocationOrganizationWay(@JsonProperty("location_id") int locationId) {
        this.locationId = locationId;
    }

    public JsonLocationOrganizationWay(LocationOrganizationWay organizationWay) {
        this.locationId = organizationWay.getLocation().getId();
    }
}
