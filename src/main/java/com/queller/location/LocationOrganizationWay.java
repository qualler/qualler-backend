package com.queller.location;

import com.queller.money.Money;
import com.queller.trainingdate.organization.OrganizationWay;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
public class LocationOrganizationWay extends OrganizationWay {

    @ManyToOne
    private Location location;

    public LocationOrganizationWay(Location location) {
        this.location = location;
    }

    public LocationOrganizationWay() {
    }

    @Override
    public Money calcDailyCosts() {
        return location.getDailyRentalCosts();
    }
}
