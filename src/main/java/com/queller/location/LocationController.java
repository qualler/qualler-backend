package com.queller.location;

import com.queller.trainingdate.DetailedJsonTrainingDate;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
@RequestMapping("/locations")
public class LocationController {

    private final LocationService locationService;

    @GetMapping
    public List<DetailedJsonLocation> listLocations() {
        return locationService.listLocations();
    }

    @GetMapping("/{locationId}")
    public Optional<DetailedJsonLocation> listLocations(@PathVariable("locationId") int locationId) {
        return locationService.findLocation(locationId);
    }

    @GetMapping("/{locationId}/training-dates")
    public List<DetailedJsonTrainingDate> listTrainingDates(@PathVariable("locationId") int locationId) {
        return locationService.findByLocation(locationId);
    }

    @PostMapping
    public void createLocation(@RequestBody JsonLocation location) {
        locationService.createLocation(location);
    }

    @PutMapping("/{locationId}")
    public void updateLocation(@PathVariable("locationId") int locationId,
                               @RequestBody JsonLocation location) {
        locationService.updateLocation(locationId, location);
    }

}
