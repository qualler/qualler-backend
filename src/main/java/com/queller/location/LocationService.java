package com.queller.location;

import com.queller.trainingdate.DetailedJsonTrainingDate;
import com.queller.trainingdate.TrainingDateRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@Validated
@AllArgsConstructor
public class LocationService {

    private final LocationsRepository locationsRepository;
    private final TrainingDateRepository trainingDateRepository;

    public void createLocation(@Valid JsonLocation jsonLocation) {
        Location location = Location.builder()
                .dailyRentalCosts(jsonLocation.getDailyRentalCosts().asMoney())
                .address(jsonLocation.getAddress().asAddress())
                .build();

        locationsRepository.save(location);
    }

    public void updateLocation(int locationId, @Valid JsonLocation jsonLocation) {
        Location location = locationsRepository.getOne(locationId);

        location.setAddress(jsonLocation.getAddress().asAddress());
        location.setDailyRentalCosts(jsonLocation.getDailyRentalCosts().asMoney());
    }

    public List<DetailedJsonLocation> listLocations() {
        return locationsRepository.findAll().stream()
                .map(DetailedJsonLocation::new)
                .collect(Collectors.toList());
    }

    public List<DetailedJsonTrainingDate> findByLocation(int locationId) {
        return trainingDateRepository.findByLocation(locationId).stream()
                .map(DetailedJsonTrainingDate::new)
                .collect(Collectors.toList());
    }

    public Optional<DetailedJsonLocation> findLocation(int locationId) {
        return locationsRepository.findById(locationId)
                .map(DetailedJsonLocation::new);
    }
}
