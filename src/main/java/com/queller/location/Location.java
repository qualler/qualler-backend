package com.queller.location;

import com.queller.money.Money;
import com.queller.address.Address;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
@Getter
@Setter
@Builder
public class Location {

    @Id
    @GeneratedValue
    private Integer id;

    private Address address;
    private Money dailyRentalCosts;

    public Location() {
    }

    public Location(Integer id, Address address, Money dailyRentalCosts) {
        this.id = id;
        this.address = address;
        this.dailyRentalCosts = dailyRentalCosts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Location)) return false;
        Location location = (Location) o;
        return Objects.equals(getId(), location.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
