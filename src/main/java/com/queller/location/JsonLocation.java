package com.queller.location;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.queller.address.JsonAddress;
import com.queller.money.JsonMoney;
import lombok.Getter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Getter
public class JsonLocation {

    @NotNull
    @Valid
    private final JsonAddress address;
    @NotNull
    @Valid
    private final JsonMoney dailyRentalCosts;

    @JsonCreator
    public JsonLocation(@JsonProperty("address") JsonAddress address,
                        @JsonProperty("daily_rental_costs") JsonMoney dailyRentalCosts) {
        this.address = address;
        this.dailyRentalCosts = dailyRentalCosts;
    }

    public JsonLocation(Location location) {
        this.address = new JsonAddress(location.getAddress());
        this.dailyRentalCosts = new JsonMoney(location.getDailyRentalCosts());
    }
}
