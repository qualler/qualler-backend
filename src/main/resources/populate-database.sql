insert
    into
        training
        (description, name, id)
    values
        (
            '',
            'Podstawy programowania w Javie',
            hibernate_sequence.nextval
        );
insert
    into
        training
        (description, name, id)
    values
        (
            '',
            'Programowanie współbieżne w Javie',
            hibernate_sequence.nextval
        );
insert
    into
        training
        (description, name, id)
    values
        (
            '',
            'Raportowanie za pomocą JasperReports',
            hibernate_sequence.nextval
        );